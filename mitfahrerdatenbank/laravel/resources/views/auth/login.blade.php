@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Benutzername:') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password:') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                                <a class="registerLink" href="http://localhost:8080/register">Registrieren</a>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.card {
    position: unset;
    padding-left: 35%;
    font-family: sans-serif;
    font-size: 35px;
    margin-top: 5%;
}

#email {
      min-width: 45%;
      min-height: 35px;
      border-radius: 5px 5px 5px 5px;
      border-color: #2678b5;
      opacity: 0.6;
}

#password {
    min-width: 45%;
    min-height: 35px;
    border-radius: 5px 5px 5px 5px;
    border-color: #2678b5;
    opacity: 0.6;
}

.form-group {
  margin-top: 15px;
}

label {
    color: #2679b5;
    font-family: 'Nunito', sans-serif;
}

.btn-primary {
   font-size: 25px;
    border-radius: 5px 5px 5px 5px;
    border-color: #2678b5;
    color: #2678b5;
    font-family: 'Nunito', sans-serif;
    background: #ffff;
    border-style: none;
}

.registerLink {
   font-size: 25px;
    margin-left: 27%;
    color: #2678b5;
    text-decoration: blink;
    font-family: 'Nunito', sans-serif;
}
</style>
@endsection
