@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Vorname, Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <!-- Nochmal überprüfen ob das Feld passt -->
                        <div class="form-group row">
                            <label for="geschlecht" class="col-md-4 col-form-label text-md-right">{{ __('Geschlecht') }}</label>

                            <div class="col-md-6">
                                <input id="geschlecht" type="geschlecht" class="form-control @error('geschlecht') is-invalid @enderror" name="geschlecht" value="{{ old('geschlecht') }}" required autocomplete="geschlecht">

                                @error('geschlecht')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- Nochmal überprüfen ob das Feld passt -->
                        <div class="form-group row">
                            <label for="alter" class="col-md-4 col-form-label text-md-right">{{ __('Alter') }}</label>

                            <div class="col-md-6">
                                <input id="alter" type="alter" class="form-control @error('alter') is-invalid @enderror" name="alter" value="{{ old('alter') }}" required autocomplete="alter">

                                @error('alter')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Password wiederholen') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Bestätigen') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.card {
    position: unset;
    padding-left: 35%;
    font-family: sans-serif;
    font-size: 35px;
    margin-top: 5%;
}

.btn-primary {
   font-size: 25px;
    border-radius: 5px 5px 5px 5px;
    border-color: #2678b5;
    color: #2678b5;
    font-family: 'Nunito', sans-serif;
    background: #ffff;
    margin-left: 17%;
}

label {
    color: #2679b5;
    font-family: 'Nunito', sans-serif;
}

.form-group {
  margin-top: 15px;
}

input {
        min-width: 45%;
        min-height: 35px;
        border-radius: 5px 5px 5px 5px;
        border-color: #2678b5;
        opacity: 0.6;
}

</style>

@endsection
