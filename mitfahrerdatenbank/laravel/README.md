# Team Kalender 

## Setup
- Install composer in container
```sh
composer install
```
- Generate key in container
```sh
php artisan key:generate
```
- Copy .env.example and rename to .env

- Run database migration 
```sh
php artisan migrate:fresh
```

###Database credentials
- host: 0.0.0.0
- database: homestead
- password: secret
- port: 33061

## Testing

```sh
php vendor/bin/phpunit
```

## Deployment
- For deploying please install deployer

```bash
curl -LO https://deployer.org/deployer.phar
mv deployer.phar /usr/local/bin/dep
chmod +x /usr/local/bin/dep
```
- Tag git revision if not done

```sh
# Get current version from .env
git tag <version>
```

```sh
dep deploy
```

## Troubleshooting

